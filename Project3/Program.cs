﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project3
{
    public abstract class  Employee
    {
        protected string empName;

        public double avgMonthlyPayment;

        public abstract double AvgMonthlyPayment();

        public string getName()
        {
            return empName;
        }

    }


    public class Employee1 : Employee {

        private double hourlyPayment;

        private int workingHours;

        public Employee1(string name, double payment, int hourse)
        {
            empName = name;
            hourlyPayment = payment;
            workingHours = hourse;
            avgMonthlyPayment = AvgMonthlyPayment();

        }

        public override double AvgMonthlyPayment()
        {
            return hourlyPayment * workingHours;
        }

        public static Employee1 readData()
        {
            Console.WriteLine("Введите имя сотрудника: ");
            string name = Console.ReadLine();
            Console.WriteLine("Введите почасовую зп сотрудника: ");
            double payment = Convert.ToDouble(Console.ReadLine());
            if (payment < 0) throw new FormatException("Число из недопустимого диапазона");
            Console.WriteLine("Введите количество рабочих часов сотрудника: ");
            int hourse = Convert.ToInt32(Console.ReadLine());
            if (hourse < 0) throw new FormatException("Число из недопустимого диапазона");

            return new Employee1(name, payment, hourse);
        }
    }


    public class Employee2 : Employee {

        private double fixedPayment;

        public Employee2(string name, double payment)
        {
            empName = name;
            fixedPayment = payment;
            avgMonthlyPayment = AvgMonthlyPayment();

        }

        public override double AvgMonthlyPayment()
        {
            return fixedPayment;
        }


        public static Employee2 readData()
        {
            Console.WriteLine("Введите имя сотрудника: ");
            string name = Console.ReadLine();
            Console.WriteLine("Введите фиксированную зп сотрудника: ");
            double payment = Convert.ToDouble(Console.ReadLine());
            if (payment < 0) throw new FormatException("Число из недопустимого диапазона");

            return new Employee2(name, payment);
        }

    }




    public class Program
    {
        static Employee[] array;      

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите количество сотрудников для внесения в список: ");

                int count = Convert.ToInt32(Console.ReadLine());

                if (count < 1) throw new FormatException("Можете внести не менее одного сотрудника");

                array = new Employee[count];

                int added = 0;
                while (added != count)
                {
                    Console.WriteLine("Введите тип оплаты нового сотрудника: \n 1 - для почасовой оплаты \n 2 - для фиксированной оплаты ");
                    try
                    {
                        int type = Convert.ToInt32(Console.ReadLine());
                        if (type == 1)
                        {
                            array[added] = Employee1.readData();
                            added++;
                        }
                        else
                        {
                            if (type == 2)
                            {
                                array[added] = Employee2.readData();
                                added++;
                            }
                            else
                            {
                                Console.WriteLine("Не допустимое значение ");
                            }
                        }
                    }catch(FormatException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }

                var result = from e1 in array
                             orderby e1.avgMonthlyPayment descending, e1.getName() ascending
                             select e1;
                int j = count;
                foreach (Employee u in result)
                {
                    Console.WriteLine("Сотрудник {0}: {1} , {2}", count - j + 1, u.getName(), u.avgMonthlyPayment);
                    j--;
                }
                Console.WriteLine("Введите количество топ-первых сотрудников");

                int firstN = Convert.ToInt32(Console.ReadLine()) - 1;
                if (firstN + 1 > count || firstN + 1 < 1) throw new FormatException("Число из недопустимого диапазона");

                Console.WriteLine("{0}", j);
                foreach (Employee u in result)
                {
                    Console.WriteLine("Сотрудник {0}: {1} , {2}", j + 1, u.getName(), u.avgMonthlyPayment);
                    if (firstN == j) break; else j++;
                }
            }catch(FormatException e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}

